const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const app = express();
const PUERTO = 3000;
const DIR_DB = "mongodb://localhost/proyecto-topicos";

mongoose.connect(DIR_DB).then((db)=>{
    console.log("Se ha establecido conexión con la base de datos");
}).catch((err)=>{
    console.log("Oh no, parece que algo a malido sal: ");
    console.log(err);
});

//rutas
const index = require("./routes/index");
const publicaciones = require("./routes/publicaciones");
const eventos = require("./routes/eventos");
const discusiones = require("./routes/discusiones");
const respuestas = require("./routes/respuestas");
const autores = require("./routes/autores");

//
//Aquí irá el código para Vue.js (supongo?)
//Aun no están configuradas las vistas 
// ¿vuejs sólo hará requests?
//

app.use(express.urlencoded({extended:true}));

//Creando las rutas
app.use("/", index);
app.use("/publicaciones", publicaciones);
app.use("/eventos", eventos);
app.use("/discusiones", discusiones);
app.use("/respuestas", respuestas);
app.use("/autores", autores);

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.listen(PUERTO, ()=>{
    console.log("Express corriendo en el puerto... "+PUERTO);
});