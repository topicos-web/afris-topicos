const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const autor_scheme = new Schema({
    nombre: {
        type: String,
        unique: true
    },
    fotoPerfil:{
        type: String,
        default: "no.png"
    }
});

module.exports = mongoose.model("autores", autor_scheme);