const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const discusion_scheme = new Schema({
    titulo: {
        type: String,
        unique: true
    },
    mensaje: String,
    fecha: Date,
    idAutor: String
});

module.exports = mongoose.model("discusiones", discusion_scheme);