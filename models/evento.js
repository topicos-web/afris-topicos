const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const evento_scheme = new Schema({
    titulo: {
        type: String,
        unique: true
    },
    descripcion: String,
    ubicacion: String,
    fecha: Date,
    hora: String,
    idAutor: String
});

module.exports = mongoose.model("eventos", evento_scheme);