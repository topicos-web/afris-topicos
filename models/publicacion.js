const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const publicacion_scheme = new Schema({
    titulo: {
        type: String,
        unique: true
    },
    contenido: String,
    fecha: Date,
    idAutor: String
});

module.exports = mongoose.model("publicaciones", publicacion_scheme);