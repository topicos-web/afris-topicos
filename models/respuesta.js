const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const respuesta_scheme = new Schema({
    titulo: String,
    mensaje: String,
    fecha: Date,
    idAutor: String,
    idDiscusion: String
});

module.exports = mongoose.model("respuestas", respuesta_scheme);