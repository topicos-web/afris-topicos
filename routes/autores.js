const express = require("express");
const router = express.Router();
const papaparse = require("papaparse");
const formidable = require("formidable");
const util = require("util");
const fs = require("fs-extra");

const {fstat} = require("fs");
const { resolve } = require("path");
 
const Autor = require("../models/autor");

//middlewares
router.use(async (req, res, next)=>{
    next();
});

router.use(async function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// aquí terminan los middlewares

// obtiene todas los autores
router.get("/", async (req, res, next)=>{
    let autores = await Autor.find();
    res.status(200).json(autores);
    res.end();
});

// obtiene un autor dada su id
router.get("/:id", async (req, res, next)=>{
    let id = req.params.id;
    let autor = await Autor.find({_id: id});
    res.status(200).json(autor);
});

router.post("/", async (req, res, next)=>{
    let autor = {
        nombre: req.body.nombre
    }
    await Autor.insertMany(autor, async function(err){
        if(!err){
            res.status(200).send("¡Todo correcto!");
        }else{
            res.status(500).send("Algo salió mal al registrar su autor: "+err);
        }
    });
});

module.exports = router;
