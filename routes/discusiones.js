const express = require("express");
const router = express.Router();
const papaparse = require("papaparse");
const formidable = require("formidable");
const util = require("util");
const fs = require("fs-extra");

const {fstat} = require("fs");
const { resolve } = require("path");

const Discusion = require("../models/discusion");
const Respuesta = require("../models/respuesta");

//middlewares
router.use(async (req, res, next)=>{
    next();
});

router.use(async function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// aquí terminan los middlewares

// obtiene todas las discusiones
router.get("/", async (req, res, next)=>{
    let discusiones = await Discusion.find({}, async function(err){
        if(!err){
            console.log("¡Todo correcto!");
        }else{
            res.status(500).send("Algo salió mal :/");
            res.end();
            return;
        }
    });
    res.status(200).json(discusiones);
    res.end();
});

//obtiene la última discusión
router.get("/ultima", async (req, res, next)=>{
    let discusion = await Discusion.find({}).sort({_id: -1}).limit(1);
    res.status(200).json(discusion);
    res.end();
});

// obtiene una discusión dada un id
router.get("/:id", async (req, res, next)=>{
    let id = req.params.id;
    let discusion = await Discusion.find({_id: id});
    res.status(200).json(discusion);
    res.end();
});

// obtiene una lista de discusiones dadas las especificaciones
// i: desde donde se empieza a buscar
// n: el número de discusiones que se desea
router.get("/:i/:n", async (req, res, next)=>{
    let i = parseInt(req.params.i, 10)-1;
    let n = parseInt(req.params.n, 10);

    let discusiones = await Discusion.find({}).limit(n);
    discusiones = discusiones.slice(i,n);

    res.status(200).json(discusiones);
    res.end();
});

// nueva discusión
router.post("/", async (req, res, next)=>{
    let nuevaDiscusion = {
        titulo: req.body.titulo,
        mensaje: req.body.mensaje,
        fecha: req.body.fecha,
        idAutor: req.body.idAutor
    }
    await Discusion.insertMany(nuevaDiscusion, function(err){
        if(!err){
            res.status("200").send("Todo bien, todo correcto!");
        }else{
            res.status("500").send("Algo salió mal: "+err)
        }
    });
});

// actualiza una discusión
router.post("/actualizar/:id", async (req, res, next)=>{
    let id = req.params.id;
    let discusion = {
        titulo: req.body.titulo,
        mensaje: req.body.mensaje,
        fecha: req.body.fecha
    }
    await Discusion.updateOne({_id: id}, discusion, async function(err){
        if(!err){
            res.status("200").send("Todo bien, todo correcto!");
        }else{
            res.status("500").send("Algo salió mal: "+err)
        }
    });
    res.end();
});

// elimina una discusión junto con todas sus respuestas
router.post("/eliminar/:id", async (req, res, next)=>{
    let id = req.params.id;
    await Discusion.deleteOne({_id: id}, async function(err){
        if(!err){
            res.status("200").send("Todo bien, todo correcto!");
        }else{
            res.status("500").send("Algo salió mal: "+err)
        }
    });
    await Respuesta.deleteMany({idDiscusion: id}, async function(err){
        if(!err){
            res.status("200").send("Todo bien, todo correcto!");
        }else{
            res.status("500").send("Algo salió mal: "+err)
        }
    });
})

module.exports = router;