const express = require("express");
const router = express.Router();
const papaparse = require("papaparse");
const formidable = require("formidable");
const util = require("util");
const fs = require("fs-extra");

const {fstat} = require("fs");

const Evento = require("../models/evento");

//middlewares
router.use(async (req, res, next)=>{
    next();
});

router.use(async function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// aquí terminan los middlewares

// Obtiene todos los eventos
router.get("/", async (req, res, next)=>{
    let eventos = await Evento.find();
    res.status(200).json(eventos);
    res.end();
});

// Obtiene los ultimos 3 eventos
router.get("/ultimo", async (req, res, next)=>{
    let eventos = await Evento.find({}).sort({_id:-1}).limit(3);
    res.status(200).json(eventos);
    res.end();
});

// Obtiene un evento por id
router.get("/:id", async (req, res, next)=>{
    let id = req.params.id;
    let evento = await Evento.find({_id: id});
    res.status(200).json(evento);
    res.end();
});

// Obtiene una lista de eventos dadas las especificaciones
// i: desde donde se empieza a buscar
// n: el número de publicaciones que se desea
router.get("/:i/:n", async (req, res, next)=>{
    
    let i = parseInt(req.params.i, 10)-1;
    let n = parseInt(req.params.n, 10);
    

    let eventos = await Evento.find({}).limit(n);
    eventos = eventos.slice(i, n);
    res.status(200).json(eventos);
    res.end();
});

// Guardar nuevo evento
router.post("/", async(req, res, next)=>{
    console.log(req.body);    
    let _titulo = req.body.titulo;
    let _ubicacion = req.body.ubicacion;
    let _fecha = req.body.fecha;
    let _hora = req.body.hora;
    let _idAutor = req.body.idAutor;
    let _descripcion = req.body.descripcion;
    await Evento.insertMany({
        titulo: _titulo,
        descripcion: _descripcion,
        ubicacion: _ubicacion,
        fecha: _fecha,
        hora: _hora,
        idAutor: _idAutor
    });
    res.redirect("http://localhost:8080/#/eventos");
});


// Actualiza un evento
router.post("/actualizar/:id", async (req, res, next)=>{
    let id = req.params.id;
    let nuevosValores = {
        titulo: req.body.titulo,
        descripcion: req.body.descripcion,
        ubicacion: req.body.ubicacion,
        fecha: req.body.fecha,
        hora: req.body.hora
    };
    await Evento.updateOne({_id: id}, nuevosValores);
    res.redirect("http://localhost:8080/#/eventos");
});

// Elimina un evento
router.post("/eliminar/:id", async (req, res, next)=>{
    let id = req.params.id;
    await Evento.deleteOne({_id: id}, async function(err){
        if(!err){
            res.status("200").send("Todo bien, todo correcto!");
        }else{
            res.status("500").send("Algo salió mal: "+err)
        }
    });
});

module.exports = router;
