const express = require("express");
const router = express.Router();
const papaparse = require("papaparse");
const formidable = require("formidable");
const util = require("util");
const fs = require("fs-extra");

const {fstat} = require("fs");
const { resolve } = require("path");
 
const Publicacion = require("../models/publicacion");

//middlewares
router.use(async (req, res, next)=>{
    next();
});

router.use(async function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// aquí terminan los middlewares

// obtiene todas las publicaciones
router.get("/", async (req, res, next)=>{
    let publicaciones = await Publicacion.find();
    res.status(200).json(publicaciones);
    res.end();
});

// obtiene la ultima publicación
router.get("/ultimo", async (req, res, next)=>{
    let publicaciones = await Publicacion.find({}).sort({_id:-1}).limit(1);
    res.status(200).json(publicaciones);
    res.end();
});

// obtiene una publicacion dada un id
router.get("/:id", async (req, res, next)=>{
    let id = req.params.id;
    let publicacion = await Publicacion.find({_id: id});
    res.status(200).json(publicacion);
    res.end();
});

// obtiene una lista de publicaciones dadas las especificaciones
// i: desde donde se empieza a buscar
// n: el número de publicaciones que se desea
router.get("/:i/:n", async (req, res, next)=>{
    
    let i = parseInt(req.params.i, 10)-1;
    let n = parseInt(req.params.n, 10);
    

    let publicaciones = await Publicacion.find({}).limit(n);
    publicaciones = publicaciones.slice(i, n);
    res.status(200).json(publicaciones);
    res.end();
});

// nueva publicación
router.post("/", async(req, res, next)=>{
    let _titulo = req.body.titulo;
    let _contenido = req.body.contenido;
    let _fecha = req.body.fecha;
    let _idAutor = req.body.idAutor;
    await Publicacion.insertMany({
        titulo: _titulo,
        contenido: _contenido,
        fecha: _fecha,
        idAutor: _idAutor
    });
    res.redirect("http://localhost:8080/#/publicaciones");
})

// actualiza una publicacion
router.post("/actualizar/:id", async (req, res, next)=>{
    let id = req.params.id;
    let nuevosValores = {
        titulo: req.body.titulo,
        contenido: req.body.contenido,
        fecha: req.body.fecha
    };
    await Publicacion.updateOne({_id: id}, nuevosValores);
    res.redirect("http://localhost:8080/#/publicaciones");
})

// elimina una publicacion
router.post("/eliminar/:id", async (req, res, next)=>{
    let id = req.params.id;
    await Publicacion.deleteOne({_id: id}, async function(err){
        if(!err){
            res.status("200").send("Todo bien, todo correcto!");
        }else{
            res.status("500").send("Algo salió mal: "+err)
        }
    });
})


module.exports = router;
