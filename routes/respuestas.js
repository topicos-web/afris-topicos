const express = require("express");
const router = express.Router();
const papaparse = require("papaparse");
const formidable = require("formidable");
const util = require("util");
const fs = require("fs-extra");

const {fstat} = require("fs");
const { resolve } = require("path");

const Discusion = require("../models/discusion");
const Respuesta = require("../models/respuesta");

//middlewares
router.use(async (req, res, next)=>{
    next();
});

router.use(async function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// aquí terminan los middlewares

// obtiene todas las respuestas dado un id de una discusion
router.get("/:idDiscusion", async (req, res, next)=>{
    let idD = req.params.idDiscusion;
    let respuestas = await Respuesta.find({idDiscusion: idD}, async function(err){
        if(!err){
            console.log("Se han solicitado todas las respuestas para la discusión con el ID: "+idD);
        }else{
            res.status(500).send("¡Algo salió mal al solicitar las respuestas para la discusión con el ID "+idD+"!");
        }
    });
    res.status(200).json(respuestas);
    res.end();
});

// obtiene una respuesta en especifico dado un id
router.get("/r/:idRespuesta", async (req, res, next)=>{
    let id = req.params.idRespuesta;
    let respuesta = await Respuesta.find({_id: id}, async function(err){
        if(!err){
            console.log("Se ha solicitado la respuesta con el ID: "+id);
        }else{
            res.status(500).send("¡Algo salió mal al solicitar la respuesta con el ID "+id+"!");
        }
    });
    res.status(200).json(respuesta);
    res.end();
});

// agrega una respuesta nueva
router.post("/", async (req, res, next)=>{
    let respuesta = {
        titulo: req.body.titulo,
        mensaje: req.body.mensaje,
        fecha: req.body.fecha,
        idAutor: req.body.idAutor,
        idDiscusion: req.body.idDiscusion
    }
    await Respuesta.insertMany(respuesta, async function(err){
        if(!err){
            let mensaje = "Se ha ingresado una respuesta para la discusión con el ID: "+respuesta.idDiscusion;
            console.log(mensaje);
            res.status(200).send(mensaje);
            res.end();
        }else{
            res.status(500).send("¡Algo salió mal al registrar la nueva respuesta!");
            res.end();
        }
    });
});

// actualiza una respuesta
router.post("/actualizar/:id", async (req, res, next)=>{
    let id = req.params.id;
    let respuesta = {
        titulo: req.body.titulo,
        mensaje: req.body.mensaje
    }
    await Respuesta.updateOne({_id: id}, respuesta, async function(err){
        if(!err){
            let mensaje = "Se ha actualizado la respuesta con el ID: "+id;
            console.log(mensaje);
            res.status(200).send(mensaje);
            res.end();
        }else{
            res.status(500).send("¡Algo salió mal al actualizar la respuesta!");
            res.end();
        }
    });
});

// elimina una respuesta
router.post("/eliminarr/:id", async (req, res, next)=>{
    let id = req.params.id;
    
    await Respuesta.deleteOne({_id: id}, async function(err){
        if(!err){
            let mensaje = "Se ha eliminado la respuesta con el ID: "+id;
            console.log(mensaje);
            res.status(200).send(mensaje);
            res.end();
        }else{
            res.status(500).send("¡Algo salió mal al eliminar la respuesta!");
            res.end();
        }
    });
});


module.exports = router;